#include <iostream>
#include <stack>
#include "Pila.h"


using namespace std;


int main(){

    string largo;

    cout << "\n\t= BIENVENIDO =" << endl;
    cout << "\n¿Cual será el largo de su pila?: ";
    getline(cin, largo);

    Pila pila = Pila(stoi(largo));

    pila.menu();

    return 0;
}
