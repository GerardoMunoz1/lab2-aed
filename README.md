# Laboratorio 2 - Unidad 1 
    - Algoritmo y estructura de datos - Ingeniería Civil en Bioinformática
    - Gerardo Muñoz
    - Universidad de Talca

Para instalar el programa, se debe ejecutar el siguiente comando dentro de la terminal.

`make`

Para correr el código, utilizar: 

`./ejercicio_pila`

Todos estos comandos deben ser usados en la ruta respectiva donde se ha descargado el repositorio.