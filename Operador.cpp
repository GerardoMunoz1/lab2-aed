#include <iostream>
#include <stack>
#include "Operador.h"

using namespace std;

Operador::Operador(){}


bool Operador::pila_vacia(int tope){

    bool vacia = 0;

    if(tope == 0){
        vacia =1;
    }

    return vacia;
}

bool Operador::pila_llena(int tope, int largo){

    bool llena = 0;

    if(tope == largo){
        llena = 1;
    }

    return llena;
}


void Operador::revisar_pila(int tope, stack<int> pila){

    cout << "\n\t> PILA <" << endl;
    int arreglo_aux[tope];


    for (int i=0; i<tope; i++){
        cout << "|" << pila.top() << "|" << endl;
        arreglo_aux[tope-i] = pila.top();
        pila.pop();
    }

    for (int i=0; i<tope; i++){
        pila.push(arreglo_aux[i]);
    }

    cout << "\nPresione [ENTER] para continuar.";
}


void Operador::add_elemento(int tope, int largo, stack<int> &pila){

    string elementos;
    string x;

    while(1){
        cout << "\n\t(Espacio disponible: " << largo-tope << ")";
        cout << "\n¿Cuantos elementos desea añadir?: ";
        getline(cin, elementos);

        if(stoi(elementos) > (largo-tope)){
            cout << "No existe espacio suficiente en la cola." << endl;
        }
        else{
            break;
        }
    }

    for(int i=0; i<stoi(elementos); i++){
        cout << "\n-------------------";
        cout << "\nElemento para ingresar a la posición [ " << tope+i+1 << " ]: ";

        getline(cin, x);
        pila.push(stoi(x));
    }
}


void Operador::quitar_elemento(stack<int> &pila, bool todos){

    if(todos){

        int aux = pila.size();
        for(int i=0; i<aux; i++){
            pila.pop();
        }
    }
    else{
        pila.pop();
    }
}
