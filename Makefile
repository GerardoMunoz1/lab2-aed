prefix=/usr/local
CC = g++

CFLAGS = -g -Wall
SRC = ej1.cpp Pila.cpp Operador.cpp
OBJ = ej1.o Pila.o Operador.o
APP = ejercicio_pila

all: $(OBJ)
	$(CC) $(CFLAGS) -o $(APP) $(OBJ)

clean:
	$(RM) $(OBJ) $(APP)

install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin

uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)
